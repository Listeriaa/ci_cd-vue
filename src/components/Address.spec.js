import { config, mount } from '@vue/test-utils'
import Address from './Address.vue'

const address = {
  title: 'My address',
  city: 'Nantes',
  zipCode: '44000',
  road: 'rue du Marchix',
  state: 'France'
}

config.global.mocks = {
  $t: (text) => text
}
describe('Address.vue', () => {
  it('renders without crashing', () => {
    const wrapper = mount(Address)
    expect(wrapper).toBeDefined()
  })
  it('renders with props', () => {
    const wrapper = mount(Address, {
      propsData: {
        address,
      }
    })
    expect(wrapper).toBeDefined()
    expect(wrapper.text()).toContain('My address')
    expect(wrapper.text()).toContain('Nantes')
    expect(wrapper.html()).toMatchSnapshot()
  })
  it('emit the \'searchAddress\' / \'editAddress\' event', async () => {
    const wrapper = mount(Address, {
      propsData: {
        address,
      },
    })
    expect(wrapper).toBeDefined()
    /**
     * find a cell and click on it
     * we add 2 for number of rows / cells
     * because we have a row and a column for Player tips
     * and because we have 0 based arrays
    */
    const wrapperButtons = wrapper.findAll('button')
    expect(wrapperButtons).toBeDefined()
    wrapperButtons.at(0).trigger('click')
    expect(wrapper.emitted()).toHaveProperty('editAddress')
    expect(wrapper.emitted()).not.toHaveProperty('searchAddress')
    expect(wrapper.emitted()['editAddress']).toHaveLength(1)
    wrapperButtons.at(1).trigger('click')
    expect(wrapper.emitted()).toHaveProperty('searchAddress')
    expect(wrapper.emitted()['searchAddress']).toHaveLength(1)
  })

})
